import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TeamView } from '../../models/presentation-models/team-view';

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamPageComponent {
  team$: Observable<TeamView>;

  constructor(private route: ActivatedRoute) {
    this.team$ = this.route.data.pipe(map((data) => data.teamData));
  }
}
