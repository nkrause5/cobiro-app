import { Inject, Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { TeamView } from '../models/presentation-models/team-view';
import { GETS_TEAM_DATA, TeamService } from './token';

@Injectable({
  providedIn: 'root',
})
export class TeamResolver implements Resolve<TeamView> {
  constructor(@Inject(GETS_TEAM_DATA) private teamService: TeamService) {}

  resolve(): Observable<TeamView> {
    return this.teamService.getTeamViewData();
  }
}
