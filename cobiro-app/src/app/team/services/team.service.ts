import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, reduce } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TeamMember } from '../models/business-models/team-member';
import { TeamResponse } from '../models/business-models/team-response';
import { TeamResponseData } from '../models/business-models/team-response-data';
import { TeamResponseDataAttribute } from '../models/business-models/team-response-data-attribute';
import { TeamView } from '../models/presentation-models/team-view';
import { TeamService } from './token';

@Injectable({
  providedIn: 'root',
})
export class HttpTeamService implements TeamService {
  constructor(private http: HttpClient) {}

  getTeamViewData(): Observable<TeamView> {
    return this.http.get<TeamResponse>(environment.apiUrl).pipe(
      map((tr: TeamResponse) => tr.data),
      map((trd: TeamResponseData[]) => trd[0].attributes),
      reduce<TeamResponseDataAttribute, TeamView>((tv, a) => {
        tv.title = a.title;
        const temp: TeamMember[] = Object.keys(a.memberCards).map((tmv) => {
          return a.memberCards[tmv];
        });
        tv.teamMembers = temp.map((tm: TeamMember) => ({
          imageUrl: tm.imageUrl.w400,
          name: tm.block.title,
          position: tm.block.description,
          email: tm.block.link,
          phoneNumber: tm.block.text,
        }));
        return tv;
      }, {} as TeamView)
    );
  }
}
