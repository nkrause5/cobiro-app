import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { TeamView } from '../models/presentation-models/team-view';

export interface TeamService {
  getTeamViewData(): Observable<TeamView>;
}

export const GETS_TEAM_DATA = new InjectionToken<TeamService>('GETS_TEAM_DATA');
