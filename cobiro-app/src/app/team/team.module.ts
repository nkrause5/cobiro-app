import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TeamMemberContainerComponent } from './components/team-member-container/team-member-container.component';
import { TitleComponent } from './components/title/title.component';
import { TeamPageComponent } from './pages/team-page/team-page.component';
import { HttpTeamService } from './services/team.service';
import { GETS_TEAM_DATA } from './services/token';

@NgModule({
  declarations: [
    TeamMemberContainerComponent,
    TeamPageComponent,
    TitleComponent,
  ],
  imports: [CommonModule],
  providers: [{ provide: GETS_TEAM_DATA, useClass: HttpTeamService }],
})
export class TeamModule {}
