import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TeamMemberView } from '../../models/presentation-models/team-member-view';

@Component({
  selector: 'app-team-member-container',
  templateUrl: './team-member-container.component.html',
  styleUrls: ['./team-member-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamMemberContainerComponent {
  @Input() teamMember: TeamMemberView;
}
