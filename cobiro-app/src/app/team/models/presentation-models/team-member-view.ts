export interface TeamMemberView {
  imageUrl: string;
  name: string;
  position: string;
  email: string;
  phoneNumber: string;
}
