import { TeamMemberView } from './team-member-view';

export interface TeamView {
  title: string;
  teamMembers: TeamMemberView[];
}
