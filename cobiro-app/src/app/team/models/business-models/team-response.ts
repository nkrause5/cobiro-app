import { TeamResponseData } from './team-response-data';

export interface TeamResponse {
  data: TeamResponseData[];
}
