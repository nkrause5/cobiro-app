export interface TeamMemberInfo {
  title: string;
  description: string;
  link: string;
  text: string;
}
