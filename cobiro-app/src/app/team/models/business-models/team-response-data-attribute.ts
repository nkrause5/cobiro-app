import { TeamMembers } from './team-members';

export interface TeamResponseDataAttribute {
  title: string;
  memberCards: TeamMembers;
}
