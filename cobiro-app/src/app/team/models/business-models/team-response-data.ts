import { TeamResponseDataAttribute } from './team-response-data-attribute';

export interface TeamResponseData {
  type: string;
  id: string;
  attributes: TeamResponseDataAttribute;
}
