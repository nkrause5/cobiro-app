import { TeamMember } from './team-member';

export interface TeamMembers {
  first: TeamMember;
  second: TeamMember;
  third: TeamMember;
}
