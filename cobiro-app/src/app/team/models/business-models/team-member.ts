import { ImageSource } from './image-source';
import { TeamMemberInfo } from './team-member-info';

export interface TeamMember {
  imageUrl: ImageSource;
  block: TeamMemberInfo;
}
