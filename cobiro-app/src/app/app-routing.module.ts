import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamPageComponent } from './team/pages/team-page/team-page.component';
import { TeamResolver } from './team/services/team.resolver';

const routes: Routes = [
  {
    path: 'home',
    component: TeamPageComponent,
    resolve: {
      teamData: TeamResolver,
    },
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
